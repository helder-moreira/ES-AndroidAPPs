package com.moreirahelder.tapmealuserapp;

import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

/**
 * Created by helder on 11/29/15.
 */
public class Reservations extends Fragment {


    static View view;
    LinearLayout reservations_layout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        container.removeAllViews();
        view = inflater.inflate(R.layout.reservations_fragment, container, false);
        reservations_layout = (LinearLayout) view.findViewById(R.id.reservations_layout);
        updateReservations();
        return view;
    }

    public int chmod(File path, int mode) {
        Class fileUtils = null;
        try {
            fileUtils = Class.forName("android.os.FileUtils");
            Method setPermissions = fileUtils.getMethod("setPermissions", String.class, int.class, int.class, int.class);
            return (Integer) setPermissions.invoke(null, path.getAbsolutePath(), mode, -1, -1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private void updateReservations() {
        if (MainActivity.token == null) {
            ((MainActivity) getActivity()).requestToken();
        } else {
            reservations_layout.removeAllViews();
            final JSONObject params = new JSONObject();
            try {
                params.put("token", MainActivity.token);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Webservice.postJSON(getActivity(), "reservationsByUser", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        JSONArray reservations = response.getJSONArray("reservations");
                        ArrayList<View> list = new ArrayList<>();
                        for (int i = 0; i < reservations.length(); i++) {
                            final JSONObject reservation = reservations.getJSONObject(i);
                            String item_name = reservation.getString("item");
                            int reserved_quantity = reservation.getInt("reserved");
                            final String reservation_date = reservation.getString("reservation_date");
                            final String meal = reservation.getString("meal");
                            String restName = reservation.getString("restaurantname");
                            final View reservation_view = getActivity().getLayoutInflater().inflate(R.layout.reservation_layout, null);
                            final int id = reservation.getInt("reservationID");
                            reservation_view.setId(id);
                            ((TextView) reservation_view.findViewById(R.id.restaurant)).setText(restName);
                            ((TextView) reservation_view.findViewById(R.id.item)).setText(item_name);
                            ((TextView) reservation_view.findViewById(R.id.meal)).setText(meal);
                            ((TextView) reservation_view.findViewById(R.id.quantity)).setText(Integer.toString(reserved_quantity));
                            ((TextView) reservation_view.findViewById(R.id.date)).setText(reservation_date);
                            View del_button = reservation_view.findViewById(R.id.delete_button);
                            del_button.setOnClickListener(new View.OnClickListener() {
                                                              @Override
                                                              public void onClick(View v) {
                                                                  try {
                                                                      JSONObject params = new JSONObject();
                                                                      params.put("token", MainActivity.token);
                                                                      params.put("reservationID", id);
                                                                      Webservice.postJSON(getActivity(), "cancelReserv", params, new AsyncHttpResponseHandler() {
                                                                          @Override
                                                                          public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                                                              String message = new String(responseBody);
                                                                              if (message.equals("\"200 OK\"\n")) {
                                                                                  Snackbar.make(reservation_view, "Successfully deleted reservation", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                                                                  updateReservations();
                                                                              } else {
                                                                                  Snackbar.make(reservation_view, "Could not delete", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                                                              }
                                                                          }

                                                                          @Override
                                                                          public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                                                              Snackbar.make(reservation_view, "Could not delete", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                                                              ((MainActivity) getActivity()).requestToken();
                                                                          }
                                                                      });
                                                                  } catch (JSONException e) {
                                                                      e.printStackTrace();
                                                                  }
                                                              }
                                                          }

                            );
                            View dl_button = reservation_view.findViewById(R.id.download_button);
                            dl_button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        JSONObject params = new JSONObject();
                                        params.put("token", MainActivity.token);
                                        params.put("reservationID", id);
                                        Webservice.postJSON(getActivity(), "getFile", params, new JsonHttpResponseHandler() {
                                            @Override
                                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                                try {
                                                    String file_content = response.getString("data").replace("\r", "");
                                                    File file = new File(getActivity().getCacheDir(), "event.ics");
                                                    FileOutputStream stream = new FileOutputStream(file);
                                                    PrintWriter pw = new PrintWriter(stream);
                                                    try {
                                                        pw.print(file_content);
                                                        pw.flush();
                                                    } finally {
                                                        try {
                                                            stream.close();
                                                            pw.close();
                                                        } catch (IOException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                    chmod(file, 0644);
                                                    MimeTypeMap map = MimeTypeMap.getSingleton();
                                                    String ext = MimeTypeMap.getFileExtensionFromUrl(file.getName());
                                                    String type = map.getMimeTypeFromExtension(ext);
                                                    if (type == null)
                                                        type = "*/*";

                                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                                    Uri data = Uri.fromFile(file);

                                                    intent.setDataAndType(data, type);
                                                    try {
                                                        getActivity().startActivity(intent);
                                                    } catch (ActivityNotFoundException e) {
                                                        Snackbar.make(reservation_view, "No handler for this type of file.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                                    }
                                                } catch (JSONException e) {
                                                    Snackbar.make(reservation_view, "Could not download event", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            list.add(reservation_view);
                        }
                        Collections.sort(list, new Comparator<View>() {
                            @Override
                            public int compare(View lhs, View rhs) {
                                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                                try {
                                    Date date1 = format.parse(((TextView) lhs.findViewById(R.id.date)).getText().toString());
                                    Date date2 = format.parse(((TextView) rhs.findViewById(R.id.date)).getText().toString());
                                    return date1.compareTo(date2);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                return 0;
                            }
                        });
                        for (View v : list) {
                            reservations_layout.addView(v);
                        }
                    } catch (JSONException e) {
                        ((MainActivity) getActivity()).requestToken();
                    }
                }
            });
        }
    }
}
