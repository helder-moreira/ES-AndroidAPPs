package com.moreirahelder.tapmealuserapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

/**
 * Created by helder on 11/29/15.
 */
public class HomeFragment extends Fragment{
    LinearLayout restaurants_layout;
    View view;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
    TextView date_text;

    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            date_text.setText(sdf.format(myCalendar.getTime()));
            updateList();
        }

    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        container.removeAllViews();
        view = inflater.inflate(R.layout.home_fragment,container, false);
        restaurants_layout = (LinearLayout) view.findViewById(R.id.restaurants_list);

        date_text = (TextView) view.findViewById(R.id.date_text);
        ImageView button_image = (ImageView) view.findViewById(R.id.set_time_button);

        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                d.getDatePicker().setMinDate(new Date().getTime());
                d.show();
            }
        };
        button_image.setOnClickListener(listener);
        updateList();
        return view;
    }

    public void updateList(){
        restaurants_layout.removeAllViews();
        Webservice.get("localization/" + MainActivity.current_location+"/"+sdf2.format(myCalendar.getTime()), null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    JSONArray restaurants = serverResp.getJSONArray("Restaurants");
                    ArrayList<View> rests = new ArrayList<>();
                    for (int i = 0; i < restaurants.length(); i++) {
                        JSONObject restaurant = restaurants.getJSONObject(i);
                        final int id = restaurant.getInt("ProviderID");
                        String name = restaurant.getString("Name");
                        float classification = (float) restaurant.getDouble("classification");
                        final String coordinates = restaurant.getString("coordinates");
                        final JSONArray menu = restaurant.getJSONArray("Menu");
                        final View restaurant_view = getActivity().getLayoutInflater().inflate(R.layout.restaurant_card, null);
                        restaurant_view.findViewById(R.id.maps_button).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + coordinates));
                                getActivity().startActivity(intent);
                            }
                        });
                        restaurant_view.setId(id);
                        restaurant_view.setTag(coordinates);
                        ((TextView) restaurant_view.findViewById(R.id.name)).setText(name);
                        final RatingBar bar = (RatingBar) restaurant_view.findViewById(R.id.rating);
                        bar.setRating(classification);
                        bar.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                    if(MainActivity.token == null){
                                        ((MainActivity)getActivity()).requestToken();
                                        return false;
                                    }
                                    final Dialog rankDialog = new Dialog(getActivity());
                                    rankDialog.setContentView(R.layout.rank_dialog);
                                    rankDialog.setCancelable(true);
                                    final RatingBar ratingBar = (RatingBar) rankDialog.findViewById(R.id.dialog_ratingbar);
                                    Button updateButton = (Button) rankDialog.findViewById(R.id.rank_dialog_button);
                                    updateButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            try {
                                                JSONObject params = new JSONObject();
                                                params.put("token", MainActivity.token);
                                                params.put("restaurantID", id);
                                                params.put("review", ratingBar.getRating());
                                                Webservice.postJSON(getActivity(), "review", params, new JsonHttpResponseHandler() {
                                                    @Override
                                                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                                        try {
                                                            float rate = (float) response.getDouble("classification");
                                                            bar.setRating(rate);
                                                        } catch (JSONException e) {
                                                            try {
                                                                String message = response.getString("200");
                                                                if(message.equals("INVALID TOKEN")){
                                                                    ((MainActivity)getActivity()).requestToken();
                                                                } else{
                                                                    Snackbar.make(restaurant_view, "Successfully rated", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                                                }
                                                            } catch (JSONException e1) {
                                                                Snackbar.make(restaurant_view, "Error rating", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                                            }
                                                        }
                                                    }
                                                });
                                                rankDialog.dismiss();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    rankDialog.show();
                                    return true;
                                }
                                return false;

                            }
                        });
                        LinearLayout l = (LinearLayout) restaurant_view.findViewById(R.id.meals);
                        for (int j = 0; j < menu.length(); j++) {
                            final View item_view = getActivity().getLayoutInflater().inflate(R.layout.mini_meal, null);
                            JSONObject item = menu.getJSONObject(j);
                            final int item_id = item.getInt("itemID");
                            final String item_name = item.getString("item");
                            final float item_price = (float) item.getDouble("price");
                            final String item_meal = item.getString("meal");
                            final String item_image = item.getString("url");
                            item_view.setId(item_id);
                            ((TextView) item_view.findViewById(R.id.title)).setText(item_name);
                            ((TextView) item_view.findViewById(R.id.meal)).setText(item_meal);
                            ((TextView) item_view.findViewById(R.id.price)).setText(Float.toString(item_price));
                            if (!item_image.equals("") && !item_image.equals("null")){
                                ImageView image_view = (ImageView) item_view.findViewById(R.id.image);
                                image_view.setBackground(null);
                                image_view.setImageBitmap(Images.StringToBitMap(item_image));
                            }
                            item_view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final Dialog dialog = new Dialog(getActivity());
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.reservation_dialog);
                                    dialog.getWindow().setLayout(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
                                    dialog.setCancelable(true);
                                    if (!item_image.equals("") && !item_image.equals("null")) {
                                        ImageView image_view = (ImageView) dialog.findViewById(R.id.image);
                                        image_view.setBackground(null);
                                        image_view.setImageBitmap(Images.StringToBitMap(item_image));
                                    }
                                    ((TextView) dialog.findViewById(R.id.title)).setText(item_name);
                                    ((TextView) dialog.findViewById(R.id.meal)).setText(item_meal);
                                    ((TextView) dialog.findViewById(R.id.price)).setText(Float.toString(item_price));
                                    Button submit = (Button) dialog.findViewById(R.id.submit_button);
                                    NumberPicker picker = (NumberPicker) dialog.findViewById(R.id.npicker);
                                    picker.setMaxValue(20);
                                    picker.setMinValue(0);
                                    submit.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String date = sdf.format(myCalendar.getTime());
                                            String hour = Integer.toString(((TimePicker) dialog.findViewById(R.id.time_picker)).getCurrentHour());
                                            String minutes = String.format("%01d", ((TimePicker) dialog.findViewById(R.id.time_picker)).getCurrentMinute());
                                            String date_hour = date + ":" + hour + ":" + minutes;
                                            if (MainActivity.token == null) {
                                                ((MainActivity) getActivity()).requestToken();
                                            } else if (((NumberPicker) dialog.findViewById(R.id.npicker)).getValue() != 0) {
                                                JSONObject params = new JSONObject();
                                                try {
                                                    params.put("token", MainActivity.token);
                                                    params.put("itemID", item_id);
                                                    params.put("restaurantID", id);
                                                    params.put("quantity", ((NumberPicker) dialog.findViewById(R.id.npicker)).getValue());
                                                    params.put("date", date_hour);
                                                    Webservice.postJSON(getActivity(), "doreservation", params, new JsonHttpResponseHandler() {
                                                        @Override
                                                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                                            try {
                                                                String message = response.getString("200");
                                                                if (message.equals("INVALID TOKEN")) {
                                                                    ((MainActivity) getActivity()).requestToken();
                                                                } else if (menu.equals("INVALID STOCK")) {
                                                                    Snackbar.make(restaurant_view, "No more quantity available for reservation", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                                                } else {
                                                                    Snackbar.make(restaurant_view, "Reservation registered", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                                                }
                                                                dialog.dismiss();
                                                            } catch (JSONException e) {
                                                                Snackbar.make(restaurant_view, "Could not make reservation", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                                                ((MainActivity) getActivity()).requestToken();
                                                            }
                                                        }
                                                    });
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    });
                                    dialog.show();
                                }
                            });
                            l.addView(item_view);
                        }
                        rests.add(restaurant_view);
                    }
                    Collections.sort(rests, new Comparator<View>() {
                        @Override
                        public int compare(View lhs, View rhs) {
                            float a = Float.parseFloat(String.valueOf(((RatingBar) lhs.findViewById(R.id.rating)).getRating()));
                            float b = Float.parseFloat(String.valueOf(((RatingBar) rhs.findViewById(R.id.rating)).getRating()));
                            return (int) (b - a);
                        }
                    });
                    for (View v : rests){
                        restaurants_layout.addView(v);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
