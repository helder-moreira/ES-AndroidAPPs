package com.moreirahelder.tapmealuserapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * Created by helder on 11/27/15.
 */
public class Images {

    public static Bitmap getRoundedCroppedImage(Bitmap bmp) {
        int widthLight = bmp.getWidth();
        int heightLight = bmp.getHeight();

        Bitmap output = Bitmap.createBitmap(widthLight, heightLight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        RectF rectF = new RectF(new Rect(0, 0, widthLight, heightLight));
        canvas.drawRoundRect(rectF, widthLight / 2, heightLight / 2, paint);
        Paint paintImage = new Paint();
        paintImage.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
        canvas.drawBitmap(bmp, 0, 0, paintImage);

        Bitmap _bmp = Bitmap.createScaledBitmap(output, 200, 200, false);
        return _bmp;
    }

    public static String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, baos);
        byte[] bytes = baos.toByteArray();
        bytes = compress(bytes);
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    public static Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            encodeByte = decompress(encodeByte);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return getRoundedCroppedImage(bitmap);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    private static byte[] compress(byte[] data)  {
        byte[] output = data;
        try {
            Deflater deflater = new Deflater();
            deflater.setInput(data);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
            deflater.finish();
            byte[] buffer = new byte[1024];
            while (!deflater.finished()) {
                outputStream.write(buffer, 0, deflater.deflate(buffer));
            }
            outputStream.close();
            output = outputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return output;

    }

    private static byte[] decompress(byte[] data) {
        byte[] output = data;
        try{
            Inflater inflater = new Inflater();
            inflater.setInput(data);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
            byte[] buffer = new byte[1024];
            while (!inflater.finished()) {
                outputStream.write(buffer, 0, inflater.inflate(buffer));
            }
            outputStream.close();
            output = outputStream.toByteArray();
        } catch (IOException | DataFormatException e){
            e.printStackTrace();
        }
        return output;

    }
}
