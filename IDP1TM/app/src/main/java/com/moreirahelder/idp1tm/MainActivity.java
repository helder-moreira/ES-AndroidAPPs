package com.moreirahelder.idp1tm;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    private String token = "";
    private String callback_uuid = "";

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                JsonHttpResponseHandler gettokenhandler = new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            JSONObject serverResp = new JSONObject(response.toString());
                            if(serverResp.getString("result").equals("success")) {
                                setToken(serverResp.getString("token"));
                                setCallback_uuid("");
                                end();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                };

                if("".equals(token) && !"".equals(callback_uuid)){
                    IDP.get("gettoken/" + callback_uuid, null, gettokenhandler);
                }
            }
        }
    }

    private void end(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra("token",token);
        setResult(RESULT_OK,returnIntent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton login = (FloatingActionButton) findViewById(R.id.login);
        final TextView username = (TextView) findViewById(R.id.username);
        final TextView password = (TextView) findViewById(R.id.password);

        final JsonHttpResponseHandler jhttphandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    String authorize_url = serverResp.getString("authorize_url");
                    String callback_uuid = serverResp.getString("callback_uuid");
                    Intent intent = new Intent(MainActivity.this, WebviewActivity.class);
                    Bundle b = new Bundle();
                    b.putString("authorize_url", authorize_url);
                    setCallback_uuid(callback_uuid);
                    intent.putExtras(b);
                    startActivityForResult(intent,1);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        final Button face_button = (Button) findViewById(R.id.face_button);
        face_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    IDP.get("signin/Facebook", null, jhttphandler);
                }
            }
        });

        final Button google_button = (Button) findViewById(R.id.google_button);
        google_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    IDP.get("signin/Google", null, jhttphandler);
                }
            }
        });

        final Button twitter_button = (Button) findViewById(R.id.twitter_button);
        twitter_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    IDP.get("signin/Twitter", null, jhttphandler);
                }
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if(!isNetworkAvailable()) {
                    return;
                }
                if("".equals(username.getText().toString()) || "".equals(password.getText().toString())) {
                    Snackbar.make(view, "Username and Password must be filled", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }else{
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonParams.put("username", username.getText().toString());
                        jsonParams.put("password", password.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    IDP.postJSON(getApplicationContext(),"signin", jsonParams, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            try {
                                JSONObject serverResp = new JSONObject(response.toString());
                                String result = serverResp.getString("result");
                                String token;
                                if ("success".equals(result)) {
                                    token = serverResp.getString("token");
                                    setToken(token);
                                    Snackbar.make(view, "Logged in", Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();
                                    end();
                                } else {
                                    Snackbar.make(view, "Invalid Credentials", Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(int errorCode, Header[] headers, String s, Throwable t) {
                            Log.d("Teste!!! Token: ", s);
                        }
                    });
                }
            }
        });
    }

    public void setToken(String token){
        this.token = token;
    }

    public void setCallback_uuid(String callback_uuid){
        this.callback_uuid = callback_uuid;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        boolean result = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        if(!result){
            Snackbar.make(findViewById(android.R.id.content), "Your offline", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
        return result;
    }
}
