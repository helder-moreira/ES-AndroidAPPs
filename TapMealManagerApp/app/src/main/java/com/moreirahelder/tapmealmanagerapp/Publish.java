package com.moreirahelder.tapmealmanagerapp;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

/**
 * Created by helder on 11/26/15.
 */
public class Publish extends Fragment implements PublishMeal{

    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
            updateMeals();
        }

    };
    EditText date_editText;
    static View view;
    ImageView image_view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        container.removeAllViews();
        view = inflater.inflate(R.layout.publish_fragment, container, false);

        date_editText = (EditText) view.findViewById(R.id.date_text);
        ImageView button_image = (ImageView) view.findViewById(R.id.set_time_button);

        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                d.getDatePicker().setMinDate(new Date().getTime());
                d.show();
            }
        };

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!date_editText.getText().toString().equals("")) {
                    PublishMealDialog d = new PublishMealDialog(getActivity(), date_editText, Publish.this);
                    d.show();
                    image_view = d.getImageView();
                } else{
                    Snackbar.make(view, "Select a date first", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            }
        });
        date_editText.setOnClickListener(listener);
        button_image.setOnClickListener(listener);
        return view;
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        date_editText.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateMeals(){
        ((LinearLayout) view.findViewById(R.id.meals)).removeAllViews();
        if (!date_editText.getText().toString().equals("")){
            JSONObject params = new JSONObject();
            try {
                params.put("restaurantID", MainActivity.current_rest);
                params.put("date", date_editText.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Webservice.postJSON(getActivity(), "getMenus", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        JSONObject serverResp = new JSONObject(response.toString());
                        JSONArray menus = serverResp.getJSONArray("Menus");
                        for (int i = 0; i < menus.length(); i++) {
                            JSONObject obj = menus.getJSONObject(i);
                            newMeal(obj.getString("item"), (float) obj.getDouble("price"), obj.getString("meal"), obj.getString("url"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    @Override
    public void newMeal(String name, float price, String meal, String image){
        LinearLayout meals = (LinearLayout) view.findViewById(R.id.meals);
        View v =  getActivity().getLayoutInflater().inflate(R.layout.mini_meal, null);
        TextView title_txt = (TextView) v.findViewById(R.id.title);
        title_txt.setText(name);
        TextView meal_txt = (TextView) v.findViewById(R.id.meal);
        meal_txt.setText(meal);
        TextView price_txt = (TextView) v.findViewById(R.id.price);
        price_txt.setText(Float.toString(price));
        if (!image.equals("") && !image.equals("null")){
            ImageView image_view = (ImageView) v.findViewById(R.id.image);
            image_view.setBackground(null);
            image_view.setImageBitmap(Images.StringToBitMap(image));
        }
        meals.addView(v);
    }

    @Override
    public void activityForResult(Intent i, int requestcode) {
        startActivityForResult(i,requestcode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10 && resultCode == getActivity().RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            image_view.setBackground(null);
            Bitmap bm_temp = Images.getRoundedCroppedImage(BitmapFactory.decodeFile(picturePath));
            String temp = Images.BitMapToString(bm_temp);
            Bitmap bm = Images.StringToBitMap(temp);
            image_view.setImageBitmap(bm);
        }
    }
}
