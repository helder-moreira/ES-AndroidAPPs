package com.moreirahelder.tapmealmanagerapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    static String token = null;
    static int current_rest = 0;
    boolean rests = false;
    static float classification;

    private static String CUSTOM_ACTION = "com.moreirahelder.idp1tm.GET_TOKEN";


    NavigationView navigationView;
    View view;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                token = data.getStringExtra("token");
                SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("token", token);
                editor.apply();
                if (!rests) {
                    updateRestaurants();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (isOnline()) {
            view = findViewById(android.R.id.content);
            final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            navigationView.getMenu().getItem(0).setChecked(true);
            SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
            token = sharedPref.getString("token", null);
            if (token == null) {
                requestToken();
            } else {
                updateRestaurants();
            }
        } else {
            new AlertDialog.Builder(MainActivity.this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("No Internet")
                    .setMessage("You need access to internet to use this APP")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MainActivity.this.finish();
                        }

                    }).show();
        }
    }

    public void requestToken() {
        Intent i = new Intent();
        i.setAction(CUSTOM_ACTION);
        startActivityForResult(i, 1);
    }

    public void updateRestaurants() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("token", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    JSONArray array = serverResp.getJSONArray("restaurants");
                    if (array.length() == 0) {
                        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("token", null);
                        editor.apply();
                        new AlertDialog.Builder(MainActivity.this)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("No Restaurants")
                                .setMessage("You do not have any restaurant. Do you want to create one?")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://46.101.14.39/")));
                                        MainActivity.this.finish();
                                    }

                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        MainActivity.this.finish();
                                    }

                                })
                                .show();
                    }
                    Menu menu = navigationView.getMenu();
                    Menu g = menu.addSubMenu("Choose Restaurant:");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        String name = obj.getString("name");
                        int id = obj.getInt("id");
                        classification = (float) obj.getDouble("classification");
                        if (i == 0) {
                            current_rest = id;
                            if (getSupportActionBar() != null) {
                                getSupportActionBar().setTitle(name);
                            }
                        }
                        g.add(1, id, id, name).setIcon(R.drawable.restaurant);
                    }
                    rests = true;
                    getFragmentManager().beginTransaction().add(R.id.fragment_placeholder, new Resume()).commit();
                } catch (JSONException e) {
                    requestToken();
                    Log.d("ERROR", "Probably token is invalid");
                }

            }
        };
        Webservice.postJSON(getApplicationContext(), "restaurants", obj, handler);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, Settings.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {
        if (current_rest == 0) {
            return true;
        }
        int id = item.getItemId();

        switch (id) {
            case R.id.resume:
                getFragmentManager().beginTransaction().add(R.id.fragment_placeholder, new Resume()).commit();
                break;
            case R.id.reservations:
                getFragmentManager().beginTransaction().add(R.id.fragment_placeholder, new Reservations()).commit();
                break;
            case R.id.publish:
                getFragmentManager().beginTransaction().add(R.id.fragment_placeholder, new Publish()).commit();
                break;
            default:
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setTitle(item.getTitle());
                }
                current_rest = id;
                getFragmentManager().beginTransaction().add(R.id.fragment_placeholder, new Resume()).commit();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
