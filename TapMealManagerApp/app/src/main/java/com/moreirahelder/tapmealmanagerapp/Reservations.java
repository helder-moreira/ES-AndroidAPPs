package com.moreirahelder.tapmealmanagerapp;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

/**
 * Created by helder on 11/26/15.
 */
public class Reservations extends Fragment {

    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
            updateReservations();
        }

    };
    EditText date_editText;

    static View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        container.removeAllViews();
        view = inflater.inflate(R.layout.reservations_fragment,container, false);

        date_editText = (EditText) view.findViewById(R.id.date_text);
        ImageView button_image = (ImageView) view.findViewById(R.id.set_time_button);

        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                d.getDatePicker().setMinDate(new Date().getTime());
                d.show();
            }
        };

        date_editText.setOnClickListener(listener);
        date_editText.setText(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
        button_image.setOnClickListener(listener);

        return view;
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        date_editText.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateReservations(){
        ((LinearLayout) view.findViewById(R.id.meals_lunch)).removeAllViews();
        ((LinearLayout) view.findViewById(R.id.meals_dinner)).removeAllViews();
        if (!date_editText.getText().toString().equals("")){
            JSONObject params = new JSONObject();
            try {
                params.put("restaurantID", MainActivity.current_rest);
                params.put("date", date_editText.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Webservice.postJSON(getActivity(), "getReservationsByDate", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        JSONObject serverResp = new JSONObject(response.toString());
                        JSONArray menus = serverResp.getJSONArray("Menus");
                        for (int i = 0; i < menus.length(); i++) {
                            JSONObject obj = menus.getJSONObject(i);
                            int id = obj.getInt("itemID");
                            int n_reservations = 0;
                            JSONArray array = obj.getJSONArray("reservations");
                            ArrayList<String[]> list = new ArrayList<>();
                            for (int j = 0; j < array.length(); j++){
                                JSONObject res = array.getJSONObject(j);
                                String[] temp = {res.getString("username"),res.getString("reservation_date").split(" ")[1], Integer.toString(res.getInt("reserved_quantity"))};
                                list.add(temp);
                                n_reservations+=res.getInt("reserved_quantity");
                            }
                            newMeal(id, obj.getString("item"), (float) obj.getDouble("price"), obj.getString("meal"), obj.getString("url"), n_reservations, list);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    public void newMeal(int id, String name, float price, String meal, String image, int n_reservations, final ArrayList<String[]> list){
        LinearLayout meals;
        if(meal.equals("Dinner")){
            meals = (LinearLayout) view.findViewById(R.id.meals_dinner);
        } else if (meal.equals("Lunch")){
            meals = (LinearLayout) view.findViewById(R.id.meals_lunch);
        } else {
            return;
        }
        View v =  getActivity().getLayoutInflater().inflate(R.layout.mini_meal_r, null);
        v.setId(id);
        v.setTag(meal);
        TextView title_txt = (TextView) v.findViewById(R.id.title);
        title_txt.setText(name);
        TextView reservation_number_txt = (TextView) v.findViewById(R.id.reservs);
        reservation_number_txt.setText(Integer.toString(n_reservations));
        TextView price_txt = (TextView) v.findViewById(R.id.price);
        price_txt.setText(Float.toString(price));
        if (!image.equals("") && !image.equals("null")){
            ImageView image_view = (ImageView) v.findViewById(R.id.image);
            image_view.setBackground(null);
            image_view.setImageBitmap(Images.StringToBitMap(image));
        }
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ReservationsDialog(view.getContext(), v, list).show();
            }
        });
        meals.addView(v);
    }
}
