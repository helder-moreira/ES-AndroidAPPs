package com.moreirahelder.tapmealmanagerapp;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

/**
 * Created by helder on 11/26/15.
 */
public class Resume extends Fragment {

    View view;
    int totalReservations = 0;
    int todayReservations = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        container.removeAllViews();
        view = inflater.inflate(R.layout.resume_fragment,container, false);
        updateMeals();
        ((RatingBar) view.findViewById(R.id.dialog_ratingBar)).setRating(MainActivity.classification);
        updateTotalReservations();
        updateTodayReservations();
        return view;
    }

    public void updateMeals(){
        JSONObject params = new JSONObject();
        try {
            params.put("restaurantID", MainActivity.current_rest);
            params.put("date", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Webservice.postJSON(getActivity(), "getMenus", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    JSONArray menus = serverResp.getJSONArray("Menus");
                    for (int i = 0; i < menus.length(); i++) {
                        JSONObject obj = menus.getJSONObject(i);
                        newMeal(obj.getString("item"), (float) obj.getDouble("price"), obj.getString("meal"), obj.getString("url"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void updateTotalReservations(){
        Webservice.get("getReservations/" + MainActivity.current_rest, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    JSONArray menus = serverResp.getJSONArray("Menus");
                    for (int i = 0; i < menus.length(); i++) {
                        JSONObject obj = menus.getJSONObject(i);
                        totalReservations += obj.getInt("reserved");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                TextView total = (TextView) view.findViewById(R.id.total_reservations);
                total.setText("" + totalReservations);
            }
        });
    }

    public void updateTodayReservations(){
        JSONObject params = new JSONObject();
        try {
            params.put("restaurantID", MainActivity.current_rest);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            params.put("date", sdf.format(new Date()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Webservice.postJSON(getActivity(), "getReservationsByDate", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    JSONArray menus = serverResp.getJSONArray("Menus");
                    for (int i = 0; i < menus.length(); i++) {
                        JSONObject obj = menus.getJSONObject(i);
                        todayReservations += obj.getInt("reserved");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                TextView total = (TextView) view.findViewById(R.id.today_reservations);
                total.setText("" + todayReservations);
            }
        });
    }

    public void newMeal(String name, float price, String meal, String image){
        LinearLayout meals = (LinearLayout) view.findViewById(R.id.meals);
        View v =  getActivity().getLayoutInflater().inflate(R.layout.mini_meal, null);
        TextView title_txt = (TextView) v.findViewById(R.id.title);
        title_txt.setText(name);
        TextView meal_txt = (TextView) v.findViewById(R.id.meal);
        meal_txt.setText(meal);
        TextView price_txt = (TextView) v.findViewById(R.id.price);
        price_txt.setText(Float.toString(price));
        if (!image.equals("") && !image.equals("null")){
            ImageView image_view = (ImageView) v.findViewById(R.id.image);
            image_view.setBackground(null);
            image_view.setImageBitmap(Images.StringToBitMap(image));
        }
        meals.addView(v);
    }

}
