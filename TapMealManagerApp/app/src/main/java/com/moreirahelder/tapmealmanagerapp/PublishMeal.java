package com.moreirahelder.tapmealmanagerapp;

import android.content.Intent;

/**
 * Created by helder on 11/27/15.
 */
public interface PublishMeal {

    void newMeal(String name, float price, String meal,String image);
    void activityForResult(Intent i, int requestcode);
}
