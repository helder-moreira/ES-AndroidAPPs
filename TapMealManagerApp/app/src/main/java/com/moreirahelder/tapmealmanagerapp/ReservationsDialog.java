package com.moreirahelder.tapmealmanagerapp;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by helder on 11/26/15.
 */
public class ReservationsDialog extends Dialog {

    Context context;
    View oldMeal;
    ArrayList<String[]> reservations;

    public ReservationsDialog(Context context, View meal, ArrayList<String[]> reservations) {
        super(context);
        this.context = context;
        this.oldMeal = meal;
        this.reservations = reservations;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.reservations_popup);
        getWindow().setLayout(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
        View meal =  findViewById(R.id.meal_landscape);
        ((ImageView) meal.findViewById(R.id.image)).setImageDrawable(((ImageView) oldMeal.findViewById(R.id.image)).getDrawable());
        ((TextView) meal.findViewById(R.id.title)).setText(((TextView) oldMeal.findViewById(R.id.title)).getText());
        ((TextView) meal.findViewById(R.id.meal)).setText((String) oldMeal.getTag());
        ((TextView) meal.findViewById(R.id.reservs)).setText(((TextView) oldMeal.findViewById(R.id.reservs)).getText());
        ((TextView) meal.findViewById(R.id.price)).setText(((TextView) oldMeal.findViewById(R.id.price)).getText());
        Collections.sort(reservations, new Comparator<String[]>() {
            @Override
            public int compare(String[] lhs, String[] rhs) {
                String[] time1 = lhs[1].split(":");
                String[] time2 = lhs[1].split(":");
                int a = Integer.parseInt(time1[0])*60 + Integer.parseInt(time1[1]);
                int b = Integer.parseInt(time2[0])*60 + Integer.parseInt(time2[1]);
                return b-a;
            }
        });
        LinearLayout reservations_layout = (LinearLayout) findViewById(R.id.reservations);
        for (String[] info : reservations){
            View v =  getLayoutInflater().inflate(R.layout.reservation_layout, null);
            ((TextView) v.findViewById(R.id.hour)).setText(info[1]);
            ((TextView) v.findViewById(R.id.username)).setText(info[0]);
            ((TextView) v.findViewById(R.id.quantity)).setText(info[2]);
            reservations_layout.addView(v);
        }
    }
}
