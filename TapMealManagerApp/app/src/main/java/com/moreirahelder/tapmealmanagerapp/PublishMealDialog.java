package com.moreirahelder.tapmealmanagerapp;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by helder on 11/26/15.
 */
public class PublishMealDialog extends Dialog implements
        android.view.View.OnClickListener {

    FloatingActionButton fab;
    View view;
    Context context;
    PublishMeal callback;

    EditText date_text;
    EditText name_text;
    EditText price_text;
    Spinner meal_spinner;
    NumberPicker quantity;
    ImageView image_view;

    public PublishMealDialog(Context context, EditText date_text, PublishMeal callback) {
        super(context);
        this.date_text = date_text;
        this.callback = callback;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.publish_popup);
        view = findViewById(R.id.dialog_layout);
        fab = (FloatingActionButton) findViewById(R.id.publish_button);
        fab.setOnClickListener(this);
        getWindow().setLayout(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
        meal_spinner = (Spinner) findViewById(R.id.meal_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.meal_array, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        meal_spinner.setAdapter(adapter);
        price_text = (EditText) findViewById(R.id.price_edittext);
        name_text = (EditText) findViewById(R.id.name_edittext);
        quantity = (NumberPicker) findViewById(R.id.npicker);
        image_view = (ImageView) findViewById(R.id.image);
        image_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity)context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
                else{
                    Intent i = new Intent(Intent.ACTION_PICK);
                    i.setType("image/*");
                    callback.activityForResult(i,10);
                }
            }
        });
        quantity.setMaxValue(20);
        quantity.setMinValue(0);
    }

    public ImageView getImageView(){
        return image_view;
    }

    @Override
    public void onClick(View v) {
        if (name_text.getText().toString().equals("")
                || price_text.getText().toString().equals("")
                || quantity.getValue() == 0) {
            Snackbar.make(view, "Only image is optional", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        } else {
            try {
                JSONObject obj = new JSONObject();
                JSONArray info_array = new JSONArray();
                JSONObject info = new JSONObject();
                info.put("token", MainActivity.token);
                info.put("providerID", MainActivity.current_rest);
                info_array.put(info);
                obj.put("info", info_array);
                JSONArray body_array = new JSONArray();
                JSONObject meal = new JSONObject();
                meal.put("price", Float.parseFloat(price_text.getText().toString()));
                meal.put("name", name_text.getText().toString());
                meal.put("quantity", quantity.getValue());
                meal.put("date", date_text.getText().toString());
                meal.put("meal", meal_spinner.getSelectedItem().toString());
                final String image;
                if(image_view.getBackground() == null){
                    image = Images.BitMapToString(((BitmapDrawable)image_view.getDrawable()).getBitmap());
                    meal.put("url",image);
                }else{
                    image = "";
                    meal.put("url","");
                }
                body_array.put(meal);
                obj.put("menu", body_array);

                Webservice.postJSON(getContext(), "replenishstock", obj, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Snackbar.make(Publish.view, "Meal added sucessfully", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        callback.newMeal(name_text.getText().toString(),Float.parseFloat(price_text.getText().toString()),meal_spinner.getSelectedItem().toString(), image);
                        dismiss();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Snackbar.make(Publish.view, "Failed to add meal.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        dismiss();
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
